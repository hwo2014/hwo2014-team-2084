package noobbot;
import java.util.Map;

/**
 * Class to deserialize from JSON init message
 */
import com.google.gson.Gson;
class GeneralInit {
	public String msgType;
	public GameInit data;
}
public class GameInit {
	public Race race;
}

class Race {
	public Track track;
	public Car[] cars;
	public Object raceSession;
}

class Track {
	public String id;
	public String name;
	public Map<String,Object>[] pieces;
	public myLane[] lanes;
	public Object startingPoint;
}
class myLane {
	public int distanceFromCenter;
	public int index;
}
class Car {
	public CarId id;
	public CarDimension dimension;
}
class CarDimension {
	public double length;
	public double width;
	public double guideFlagPosition;
}
