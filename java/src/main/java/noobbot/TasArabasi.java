package noobbot;

import java.util.Map;

public class TasArabasi implements ITasArabasi{
	private GameInit gameInit;
	private TATrack[] tracks;
	//private double cumulativeTrackLengths[];
	private double position;
	private double velocity;
	private double lastPosition;
	private int lastPieceIndex;
	private int[] accelerate;
	private double[] maxSpeed;
	private boolean crashed;
	//private CarLogger logger;
	
	public TasArabasi()
	{}
	
	public void init(GameInit gi)
	{
		position=0;
		lastPosition=0;
		velocity=0;
		gameInit=gi;
		Map<String,Object>[] pieces=gi.race.track.pieces;
		tracks=new TATrack[pieces.length];
		accelerate=new int[pieces.length];
		maxSpeed=new double[pieces.length];
		crashed=false;
		
		//cumulativeTrackLengths=new double[pieces.length+1];
		for(int i=0;i<pieces.length;i++)
		{
			tracks[i]=new TATrack();
			if(pieces[i].containsKey("length"))
			{
				tracks[i].trackType=true;//straight
				tracks[i].length=(double)pieces[i].get("length");
				accelerate[i]=1;
				//cumulativeTrackLengths[i+1]=(double)pieces[i].get("length");
			}
			else
			{
				tracks[i].radius=(double)pieces[i].get("radius");
				tracks[i].angle=(double)pieces[i].get("angle");
				tracks[i].length=Math.abs(2*Math.PI*tracks[i].radius*tracks[i].angle/360.0);
				accelerate[i]=0;
				accelerate[(i-1+pieces.length)%pieces.length]=-1;
				//cumulativeTrackLengths[i+1]=Math.abs(2*Math.PI*tracks[i].radius*tracks[i].angle/360.0);
			}
			if(pieces[i].containsKey("switch"))
				tracks[i].trackSwitch=true;
		}
		
		//Estimate max turning speeds
		int N=tracks.length;
		for(int i=0;i<N;i++)
		{
			if(!tracks[i].trackType)//curved
			{
				double ratio=tracks[i].radius/Math.abs(tracks[i].angle);
				//maxSpeed[i]=(ratio-1.11111111)*2.51+3.87;//Finland 7.92 8.08
				maxSpeed[i]=(ratio-1.11111111)*2.4+3.85;
				//maxSpeed[i]=(ratio-1.11111111)*2.4+3.7;//Germany 11.32
				if(maxSpeed[i]>10)
					maxSpeed[i]=10;
			}
			else
			{
				maxSpeed[i]=10.0;
			}
			
		}
		
		//for(int i=2;i<cumulativeTrackLengths.length;i++)
		//	cumulativeTrackLengths[i]+=cumulativeTrackLengths[i-1];
		//logger=new CarLogger("dtsp_"+System.currentTimeMillis()+".txt");
		System.out.println(gi);
	}
	
	public void Update(CarPosition[] positions)
	{
		CarPosition p=positions[0];
		//position=p.piecePosition.inPieceDistance+cumulativeTrackLengths[(int)p.piecePosition.pieceIndex];
		position=p.piecePosition.inPieceDistance;
		if((int)p.piecePosition.pieceIndex!=lastPieceIndex)
			lastPieceIndex=(int)p.piecePosition.pieceIndex;
		else
			velocity=position-lastPosition;
		//if(!crashed)
		//	System.out.printf("pos:%10f v:%10f lpos:%10f ipd:%10f pi:%d lap:%d a[i]:%d\n",position,velocity,lastPosition,p.piecePosition.inPieceDistance,(int)p.piecePosition.pieceIndex,(int)p.piecePosition.lap,accelerate[lastPieceIndex]);
		lastPosition=position;
		
	}
	public double calculatedThrottle()
	{
		if(crashed)
			return 0;
		double thr;
		//v3
		int nCP=nextCurvedPiece(lastPieceIndex);
		double dTSD=distanceToSpeedDown(velocity, maxSpeed[nCP]);
		double dTP=distanceToPiece(lastPieceIndex, lastPosition, nCP);
		
		if(velocity > maxSpeed[nCP] && dTSD+10>dTP)
			thr=0;
		else
			thr=maxSpeed[lastPieceIndex]/10;
		//System.out.println("thr:"+thr+" ncP:"+nCP+" dTSD:"+dTSD+" dTP:"+dTP);
		String str = String.format("thr:%5f pi:%2d nCP:%2d pos:%7f dTSD:%5f dTP:%7f v:%5f ms:%5f msNCP:%5f",
						thr, lastPieceIndex, nCP, lastPosition, dTSD, dTP,
						velocity, maxSpeed[lastPieceIndex], maxSpeed[nCP]);
		//logger.Log(str);
		System.out.println(str);
		return thr;
		
		/*v2
		double nextTrackSpeed=maxSpeed[(lastPieceIndex+1)%maxSpeed.length];
		if(nextTrackSpeed!=1.0)
		{
			if(velocity<nextTrackSpeed)
				thr=nextTrackSpeed/10;
			else
				thr=0;
		}
		else
		{
			thr=maxSpeed[lastPieceIndex];
		}
			
		
		System.out.println("thr:"+thr+" mS:"+maxSpeed[lastPieceIndex]+" nTS:"+nextTrackSpeed);
		
		return thr;
		*/
		/*
		double maxRotationSpeed=4.7;
		double speedToReduce=4.3;
		if(accelerate[lastPieceIndex]==0)
		{
			//if(velocity>6 && velocity<6.5)
			return maxRotationSpeed/10;
		}
		else if(accelerate[lastPieceIndex]==-1)
		{
			if(velocity<speedToReduce)
				return speedToReduce/10;
			else return 0;
		}
		else
		{
			return 1;
		}*/
		//return 0.65;
	}
	public void crash()
	{
		crashed=true;
	}
	public void spawn()
	{
		crashed=false;
	}
	public double distanceToSpeedDown(double velocity,double turningVelocity)
	{
		double distance=0;
		if(velocity>turningVelocity)
			distance=0;
		while(velocity>turningVelocity)
		{
			distance+=velocity;
			velocity*=0.98;
		}
		return distance;
	}
	
	public int nextCurvedPiece(int pieceIndex)
	{
		for(int i=1;i<tracks.length;i++)
			if(!tracks[(i+pieceIndex)%tracks.length].trackType)
				return (i+pieceIndex)%tracks.length;
		return -1;
	}
	public double distanceToPiece(int pieceIndex,double inPieceDistance,int destinationPieceIndex)
	{
		double distance=0;//tracks[pieceIndex].length-inPieceDistance;
		if(pieceIndex<destinationPieceIndex)
		for(int i=pieceIndex;i<destinationPieceIndex;i++)
			distance+=tracks[i].length;
		else
			for(int i=pieceIndex;i<destinationPieceIndex+tracks.length;i++)
				distance+=tracks[i%tracks.length].length;
		distance-=inPieceDistance;
		return distance;
	}
}



class TATrack {
	public boolean trackType;
	public double length;
	public double radius;
	public double angle;
	public boolean trackSwitch;
	public TATrack()
	{}
}
