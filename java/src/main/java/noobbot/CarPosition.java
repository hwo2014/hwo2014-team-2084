package noobbot;

public class CarPosition {
	public CarId id;
	public double angle;
	public PiecePosition piecePosition;
}

class CarId {
	public String name;
	public String color;
}

class PiecePosition {
	public double pieceIndex;
	public double inPieceDistance;
	public Lane lane;
	public double lap;
}

class Lane {
	public double startIndex;
	public double endIndex;
	
}